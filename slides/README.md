# Setup

Rename the following files:

* REF-talk_name.tex to the name you would like to have
* slides/REF-title.tex -> slides/title.tex
* slides/REF-slides.tex -> slides/slides.tex
* slides/REF-slides-sec0-foreword.tex -> slides/REF-slides-sec0-foreword.tex
* slides/REF-slides-sec1.tex -> slides/REF-slides-sec1.tex
* slides/REF-slides-sec-backup.tex -> slides/slides-sec-backup.tex

Edit as needed.
